FROM python:3.11

WORKDIR app/

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements/base_req.txt .

RUN pip install --no-cache-dir --upgrade -r base_req.txt

COPY . .

EXPOSE 8000

CMD ["python3", "-m", "app.main"]