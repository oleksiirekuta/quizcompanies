import os
from pydantic_settings import BaseSettings
from pydantic import PostgresDsn, RedisDsn
from dotenv import load_dotenv

load_dotenv()


class FastAPIConfig(BaseSettings):
    DEBUG: bool = os.getenv("DEBUG")
    HOST: str = "0.0.0.0"
    PORT: int = 8000
    RELOAD: bool = True

    ORIGINS: list = [
        "http://localhost",
        "http://localhost:8000",
        "http://0.0.0.0:8000",
        "https://romanxeo.github.io/internship-token/",
        "http://localhost:3000",
        "https://romanxeo.github.io",
    ]

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = "app_"
        case_sensitive = True


class DBConfig(BaseSettings):
    DATABASE_URL: PostgresDsn = os.getenv("DATABASE_URL")
    DB_USER: str = os.environ.get("DB_USER")
    DB_PASS: str = os.environ.get("DB_PASS")
    DB_HOST: str = os.environ.get("DB_HOST")
    DB_PORT: int = os.environ.get("DB_PORT")
    DB_NAME: str = os.environ.get("DB_NAME")

    class Config:
        env_prefix = "DB_"


class RedisConfig(BaseSettings):
    REDIS_URL: RedisDsn = os.getenv("REDIS_URL")


class AuthConfig(BaseSettings):
    ISSUER: str = os.getenv("ISSUER")
    SECRET_KEY: str = os.getenv("SECRET_KEY")
    ALGORITHM: str = os.getenv("ALGORITHM")
    API_AUDIENCE: str = os.getenv("API_AUDIENCE")
    AUTH0_DOMAIN: str = os.getenv("AUTH0_DOMAIN")
    AUTH0_CLIENT_ID: str = os.getenv("AUTH0_CLIENT_ID")
    AUTH0_SIGNIN_SECRET: str = os.getenv("AUTH0_SIGNIN_SECRET")
    AUTH0_CLIENT_SECRET: str = os.getenv("AUTH0_CLIENT_SECRET")
    ACCESS_TOKEN_EXPIRE_MINUTES: int = os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES")


app_settings = FastAPIConfig()
db_settings = DBConfig()
redis_settings = RedisConfig()
auth_settings = AuthConfig()
