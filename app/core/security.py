from datetime import timedelta, datetime
import jwt
import bcrypt
import logging

from app.core.config import auth_settings

logger = logging.getLogger(__name__)


class SecurityUtils:
    @staticmethod
    def hash_password(password: str) -> str:
        # Hash the password
        hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
        return hashed_password.decode("utf-8")

    @staticmethod
    def verify_password(plain_password: str, hashed_password: str) -> bool:
        # Verify a plain password against a hashed password
        return bcrypt.checkpw(
            plain_password.encode("utf-8"), hashed_password.encode("utf-8")
        )

    @staticmethod
    def create_access_token(data: dict):
        to_encode = data.copy()
        expire = datetime.utcnow() + timedelta(
            minutes=auth_settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
        to_encode.update({"exp": expire})
        return jwt.encode(
            to_encode, auth_settings.SECRET_KEY, algorithm=auth_settings.ALGORITHM
        )
