import uuid
from typing import List

from pydantic import BaseModel


class TunedModel(BaseModel):
    class Config:
        from_attributes = True


class CompanyCreate(BaseModel):
    company_name: str | None
    company_title: str
    company_description: str | None
    company_city: str | None
    company_phone: str | None
    company_links: str | None
    company_avatar: str | None
    is_visible: bool = True


class CompanyUpdate(TunedModel):
    company_name: str | None = None
    company_description: str | None = None
    is_visible: bool = True


class CompanyDetailResponse(TunedModel):
    company_id: int
    company_name: str | None = None
    company_title: str
    company_description: str | None = None
    company_city: str | None = None
    company_phone: str | None = None
    company_links: str | None = None
    company_avatar: str | None = None
    is_visible: bool


class CompaniesListResponse(TunedModel):
    companies: List[CompanyDetailResponse]
