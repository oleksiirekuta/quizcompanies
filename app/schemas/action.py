from typing import List

from pydantic import BaseModel, EmailStr


class TunedModel(BaseModel):
    class Config:
        from_attributes = True


class BaseAction(BaseModel):
    action: str | None = None
    status: str | None = None
    company_id: int


class AcceptAction(BaseModel):
    company_id: int
    status: str | None = "pending"


class SendInvite(TunedModel):
    receivers: List[EmailStr]


class ActionDetailResponse(TunedModel):
    action: str | None = None
    status: str | None = None
    company_id: int


class Invitations(TunedModel):
    invitations: List[ActionDetailResponse]
