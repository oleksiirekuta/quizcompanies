import uuid
import re
import logging

from fastapi import HTTPException
from pydantic import BaseModel, EmailStr, field_validator
from typing import List, Optional

logger = logging.getLogger(__name__)

LETTER_MATCH_PATTERN = re.compile(r"^[A-zА-я\-]+$")


class TunedModel(BaseModel):
    class Config:
        from_attributes = True


class UserBase(TunedModel):
    user_id: uuid.UUID
    user_firstname: str
    user_lastname: str
    user_email: EmailStr
    user_status: bool
    user_city: str
    user_phone: str
    user_links: str
    user_avatar: str


class UserSignUpRequestModel(BaseModel):
    user_firstname: str
    user_lastname: str
    user_email: EmailStr
    hashed_password: str
    user_city: Optional[str]
    user_phone: Optional[str]
    user_links: Optional[str]
    user_avatar: Optional[str]

    @field_validator("user_firstname")
    def validate_name(cls, value):
        if not LETTER_MATCH_PATTERN.match(value):
            logger.info(f"Name should contains only letters: {value}")
            raise HTTPException(
                status_code=422, detail="Name should contains only letters"
            )
        return value

    @field_validator("user_lastname")
    def validate_surname(cls, value):
        if not LETTER_MATCH_PATTERN.match(value):
            logger.info(f"Name should contains only letters: {value}")
            raise HTTPException(
                status_code=422, detail="Surname should contains only letters"
            )
        return value


class UserUpdateRequestModel(TunedModel):
    user_firstname: Optional[str] = "Unknown"
    user_lastname: Optional[str] = "Unknown"


class UserSignInRequestModel(TunedModel):
    user_email: EmailStr
    hashed_password: str


class UsersListResponse(TunedModel):
    users: List[UserBase]


class UserDetailResponse(UserBase):
    pass


class PaginationParams(BaseModel):
    limit: int = 10
    page: int = 1
