import enum
import uuid
from typing import TYPE_CHECKING, List

from sqlalchemy import String, Boolean, UniqueConstraint, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import Mapped, relationship, mapped_column

from app.models.base import TimestampedModel
from app.db.base_class import Base
from app.models.mixins import UserRelationMixin

if TYPE_CHECKING:
    from app.models import Company, UsersCompaniesActions


class StatusTypeEnum(enum.Enum):
    active = "active"
    inactive = "inactive"
    deleted = "deleted"


class User(TimestampedModel, Base):
    __tablename__ = "users"
    user_id: Mapped[uuid.UUID] = mapped_column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4
    )
    user_firstname: Mapped[str] = mapped_column(
        String(64)
    )
    user_lastname: Mapped[str] = mapped_column(
        String(64)
    )
    user_email: Mapped[str] = mapped_column(
        String(64),
        unique=True
    )
    user_status: Mapped[bool] = mapped_column(
        Boolean, default=True
    )
    user_city: Mapped[str | None] = mapped_column(
        String(64)
    )
    user_phone: Mapped[str | None] = mapped_column(
        String(64)
    )
    user_links: Mapped[str | None] = mapped_column(
        String(128),
        nullable=True,
        default="",
        server_default=""
    )
    user_avatar: Mapped[str | None] = mapped_column(
        String(),
        nullable=True,
        default="",
        server_default=""
    )
    hashed_password: Mapped[str] = mapped_column(
        String(64),
        nullable=False
    )
    is_superuser: Mapped[bool] = mapped_column(
        Boolean,
        default=False,
        nullable=True
    )

    companies: Mapped[List["Company"] | None] = relationship(
        back_populates="user"
    )
    actions: Mapped[List["UsersCompaniesActions"] | None] = relationship(
        secondary="user_action",
        back_populates="users"
    )
    participant_company: Mapped[List["Company"] | None] = relationship(
        secondary="participant_company",
        back_populates="participants"
    )

    @property
    def superuser(self) -> Mapped[bool]:
        return self.is_superuser


class UserActionAssociation(Base, UserRelationMixin):
    __tablename__ = "user_action"
    __table_args__ = (
        UniqueConstraint(
            "user_id",
            "action_id",
            name="idx_unique_user_action"
        ),
    )

    id: Mapped[int] = mapped_column(
        primary_key=True
    )
    action_id: Mapped[int] = mapped_column(
        ForeignKey("user_company_action.action_id")
    )
