from sqlalchemy import (
    Integer, String, Boolean, Text, UniqueConstraint, ForeignKey
)
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base_class import Base
from app.models.base import TimestampedModel
from app.models.mixins import UserRelationMixin
from app.models import User


class Company(TimestampedModel, UserRelationMixin, Base):
    __tablename__ = "companies"
    company_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    company_name: Mapped[str | None] = mapped_column(
        String(32), default="", server_default=""
    )
    company_title: Mapped[str] = mapped_column(String(32))
    company_description: Mapped[str | None] = mapped_column(
        Text, default="", server_default=""
    )
    company_city: Mapped[str | None] = mapped_column(
        String(64), default="", server_default=""
    )
    company_phone: Mapped[str | None] = mapped_column(
        String(13), default="", server_default=""
    )
    company_links: Mapped[str | None] = mapped_column(
        String(128), default="", server_default=""
    )
    company_avatar: Mapped[str | None] = mapped_column(
        String(64), default="", server_default=""
    )
    is_visible: Mapped[bool] = mapped_column(Boolean)

    _user_back_populates = "companies"

    participants: Mapped[list["User"] | None] = relationship(
        secondary="participant_company",
        back_populates="participant_company"
    )


class CompanyParticipantsAssociation(Base, UserRelationMixin):
    __tablename__ = "participant_company"
    __table_args__ = (
        UniqueConstraint(
            "user_id",
            "company_id",
            name="idx_unique_user_company"
        ),
    )

    id: Mapped[int] = mapped_column(
        primary_key=True
    )
    company_id: Mapped[int] = mapped_column(
        ForeignKey("companies.company_id")
    )
