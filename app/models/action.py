import enum
from typing import TYPE_CHECKING, List

from sqlalchemy import Enum, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.db.base_class import Base
from app.models.base import TimestampedModel

if TYPE_CHECKING:
    from app.models import User


class ActionTypeEnum(enum.Enum):
    invitation = "invitation"
    request = "request"
    quit = "quit"


class StatusTypeEnum(enum.Enum):
    pending = "pending"
    accepted = "accepted"
    declined = "declined"


class UsersCompaniesActions(TimestampedModel, Base):
    __tablename__ = "user_company_action"

    action_id: Mapped[int] = mapped_column(primary_key=True)
    action: Mapped[enum.Enum] = mapped_column(
        Enum(ActionTypeEnum), nullable=True
    )
    status: Mapped[enum.Enum] = mapped_column(
        Enum(StatusTypeEnum), nullable=False,
        default=StatusTypeEnum.pending
    )
    company_id: Mapped[int] = mapped_column(
        ForeignKey("companies.company_id")
    )
    users: Mapped[List["User"]] = relationship(
        secondary="user_action",
        back_populates="actions"
    )
