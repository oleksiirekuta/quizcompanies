import redis.asyncio as redis
from fastapi import Depends, APIRouter
from typing import Any

from app.dependencies.custom_dependencies import get_redis_client

import logging

logger = logging.getLogger(__name__)

router = APIRouter()


@router.get("/")
def healthcheck() -> Any:
    return {"status_code": 200, "detail": "ok", "result": "working"}


@router.get("/redis-healthcheck", response_model=dict)
async def redis_healthcheck(
    redis_client: redis.Redis = Depends(get_redis_client),
) -> Any:
    try:
        result = await redis_client.info()
        return {"message": "Redis client is healthy!", "info": result}
    except Exception as e:
        logger.error(f"Redis connection: {e}")
        return {"message": "Redis client has some problem :(", "error": str(e)}
