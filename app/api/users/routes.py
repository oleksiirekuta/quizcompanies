import uuid
from typing import List

from app import schemas
from app.api.auth.routes import oauth2_scheme, token_auth_scheme
from app.services.user_manager import UserManager, AuthService
from app.dependencies.custom_dependencies import get_user_manager, get_auth_service

from fastapi import Depends, APIRouter

router = APIRouter()


@router.post("/", response_model=schemas.UserBase)
async def create_user(
    body: schemas.UserSignUpRequestModel,
    user_manager: UserManager = Depends(get_user_manager),
):
    return await user_manager.create_new_user(body)


@router.get("/{user_id}", response_model=schemas.UserDetailResponse)
async def get_user_by_id(
    user_id: uuid.UUID, user_manager: UserManager = Depends(get_user_manager)
):
    return await user_manager.get_user_by_id(user_id)


@router.get("/", response_model=List[schemas.UserBase])
async def get_all_users(
    pagination_params: schemas.PaginationParams = Depends(),
    user_manager: UserManager = Depends(get_user_manager),
):
    return await user_manager.get_all(pagination_params)


@router.patch("/{user_id}", response_model=schemas.UserBase)
async def update_user(
    user_id: uuid.UUID,
    body: schemas.UserUpdateRequestModel,
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    user_manager: UserManager = Depends(get_user_manager),
    auth_service: AuthService = Depends(get_auth_service),
):
    current_user = await auth_service.get_current_user_from_token(token)
    return await user_manager.update_user(user_id, body, current_user)


@router.put("/{user_id}/change-password", response_model=schemas.UserBase)
async def update_user_password(
    user_id: uuid.UUID,
    old_password: str,
    new_password: str,
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
    user_manager: UserManager = Depends(get_user_manager),
):
    current_user = await auth_service.get_current_user_from_token(token)
    return await user_manager.update_user_password(
        user_id, old_password, new_password, current_user
    )


@router.delete("/{user_id}")
async def delete_user(
    user_id: uuid.UUID,
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
    user_manager: UserManager = Depends(get_user_manager),
):
    current_user = await auth_service.get_current_user_from_token(token)
    await user_manager.delete_user(user_id, current_user)
    return {"message": f"User {user_id} deleted successfully"}
