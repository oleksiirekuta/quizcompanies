import uuid

from fastapi.security import (
    OAuth2PasswordBearer, HTTPBearer
)

from app.services import ActionService
from app.services.user_manager import AuthService
from app.dependencies.custom_dependencies import (
    get_auth_service, get_action_service
)
from app import schemas

from fastapi import Depends, APIRouter

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/token")
token_auth_scheme = HTTPBearer()


@router.post(
    "/{company_id}/send-invitation/"
)
async def send_invitation(
        company_id: int,
        body: schemas.SendInvite,
        token: str = Depends(oauth2_scheme),
        auth0token: str = Depends(token_auth_scheme),
        auth_service: AuthService = Depends(get_auth_service),
        action_service: ActionService = Depends(get_action_service),
):
    return await action_service.send_invite(
        current_user=await auth_service.get_current_user_from_token(token),
        receivers_email=body.receivers,
        company_id=company_id
    )


@router.get("/{user_id}/check-invitation/"
            )
async def check_invitation(
        token: str = Depends(oauth2_scheme),
        auth0token: str = Depends(token_auth_scheme),
        auth_service: AuthService = Depends(get_auth_service),
        action_service: ActionService = Depends(get_action_service),
):
    return await action_service.check_invitation(
        current_user=await auth_service.get_current_user_from_token(token)
    )


@router.get("/{user_id}/cancel-invitation/"
            )
async def cancel_invitation(
        from_company: int,
        invitation_receiver: uuid.UUID,
        token: str = Depends(oauth2_scheme),
        auth0token: str = Depends(token_auth_scheme),
        auth_service: AuthService = Depends(get_auth_service),
        action_service: ActionService = Depends(get_action_service),
):
    return await action_service.cancel_invitation(
        from_company=from_company,
        current_user=await auth_service.get_current_user_from_token(token),
        invitation_receiver=invitation_receiver
    )


@router.get("/{user_id}/accept-invitation/"
            )
async def accept_invitation(
        company_id_sender: int,
        token: str = Depends(oauth2_scheme),
        auth0token: str = Depends(token_auth_scheme),
        auth_service: AuthService = Depends(get_auth_service),
        action_service: ActionService = Depends(get_action_service),
):
    return await action_service.accept_invitation(
        company_id_sender=company_id_sender,
        current_user=await auth_service.get_current_user_from_token(token)
    )
