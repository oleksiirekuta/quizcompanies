from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer, HTTPBearer

from app.services.user_manager import AuthService
from app.dependencies.custom_dependencies import get_auth_service
from app import schemas

from fastapi import Depends, APIRouter

login_router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login/token")
token_auth_scheme = HTTPBearer()


@login_router.post("/token", response_model=schemas.Token)
async def default_login_for_access_token(
    form_data: OAuth2PasswordRequestForm = Depends(),
    auth_service: AuthService = Depends(get_auth_service),
):
    access_token = await auth_service.authenticate_user(
        user_email=form_data.username, password=form_data.password
    )

    return {"access_token": access_token, "token_type": "Bearer"}


@login_router.get("/me", response_model=schemas.UserBase)
async def profile(
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
):
    return await auth_service.get_current_user_from_token(token)
