from typing import List

from app import schemas

from fastapi import Depends, APIRouter

from app.api.auth.routes import token_auth_scheme, oauth2_scheme
from app.dependencies import get_company_service, get_auth_service
from app.services import CompanyService, AuthService

router = APIRouter()


@router.post("/", response_model=schemas.CompanyDetailResponse)
async def create_company(
    body: schemas.CompanyCreate,
    company_service: CompanyService = Depends(get_company_service),
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
):
    current_user = await auth_service.get_current_user_from_token(token)
    return await company_service.create_new_company(current_user, body)


@router.patch("/{company_id}", response_model=schemas.CompanyDetailResponse)
async def update_company(
    company_id: int,
    body: schemas.CompanyUpdate,
    company_service: CompanyService = Depends(get_company_service),
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
):
    current_user = await auth_service.get_current_user_from_token(token)
    return await company_service.update_company(
        current_user=current_user, company_id=company_id, body=body
    )


@router.get("/{title}", response_model=schemas.CompanyDetailResponse)
async def get_by_title(
    title: str, company_service: CompanyService = Depends(get_company_service)
):
    return await company_service.get_company_by_title(title)


@router.get("/", response_model=List[schemas.CompanyDetailResponse])
async def get_list_companies(
    pagination_params: schemas.PaginationParams = Depends(),
    company_service: CompanyService = Depends(get_company_service),
):
    return await company_service.get_list(pagination_params=pagination_params)


@router.get(
    "/{user_id}/my-companies",
    response_model=List[schemas.CompanyDetailResponse]
)
async def get_my_companies(
    pagination_params: schemas.PaginationParams = Depends(),
    company_service: CompanyService = Depends(get_company_service),
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
):
    current_user = await auth_service.get_current_user_from_token(token)
    return await company_service.get_my_companies(
        pagination_params=pagination_params, current_user=current_user.user_id
    )


@router.delete("/{company_id}")
async def delete_company(
    company_id: int,
    company_service: CompanyService = Depends(get_company_service),
    token: str = Depends(oauth2_scheme),
    auth0token: str = Depends(token_auth_scheme),
    auth_service: AuthService = Depends(get_auth_service),
):
    current_user = await auth_service.get_current_user_from_token(token)
    await company_service.delete_company(
        current_user=current_user, company_id=company_id
    )
    return {"message": f"Company {company_id} deleted successfully"}
