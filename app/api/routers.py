from fastapi import APIRouter

from app.api.healthcheck import router as healthcheck_router
from app.api.users.routes import router as user_router
from app.api.auth.routes import login_router
from app.api.companies.routes import router as company_router
from app.api.actions.routes import router as actions_router

api_router = APIRouter()

api_router.include_router(
    healthcheck_router, tags=["healthcheck"]
)
api_router.include_router(
    user_router, prefix="/users", tags=["users"]
)
api_router.include_router(
    login_router, prefix="/login", tags=["login"]
)
api_router.include_router(
    company_router, prefix="/companies", tags=["companies"]
)
api_router.include_router(
    actions_router, prefix="/actions", tags=["actions"]
)
