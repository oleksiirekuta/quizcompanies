from app.services import UserManager, AuthService, CompanyService, ActionService
from app.core.config import redis_settings
from app.db.session import async_session

from fastapi import Depends
from typing import Generator
from sqlalchemy.ext.asyncio import AsyncSession
import redis.asyncio as redis

import logging

logger = logging.getLogger(__name__)


async def get_redis_client():
    return await redis.Redis.from_url(str(redis_settings.REDIS_URL))


async def get_db() -> Generator:
    session: AsyncSession = async_session()
    try:
        yield session
    finally:
        await session.close()


async def get_user_manager(db: AsyncSession = Depends(get_db)) -> UserManager:
    return UserManager(db)


async def get_auth_service(db: AsyncSession = Depends(get_db)) -> AuthService:
    return AuthService(db)


async def get_company_service(
        db: AsyncSession = Depends(get_db)
) -> CompanyService:
    return CompanyService(db)


async def get_action_service(
        db: AsyncSession = Depends(get_db)
) -> ActionService:
    return ActionService(db)
