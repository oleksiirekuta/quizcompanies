from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from app.core.config import db_settings

engine = create_async_engine(str(db_settings.DATABASE_URL), future=True, echo=True)

async_session = sessionmaker(class_=AsyncSession, expire_on_commit=False, bind=engine)
