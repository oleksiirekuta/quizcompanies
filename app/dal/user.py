import uuid
import logging
from typing import Optional

from fastapi import HTTPException
from sqlalchemy import delete
from sqlalchemy.future import select

from app.dal.base import BaseDAL
from app.models import User
from app.core.security import SecurityUtils
from app.schemas import PaginationParams, UserSignUpRequestModel

logger = logging.getLogger(__name__)


class UserDAL(BaseDAL):
    async def create(self, body: UserSignUpRequestModel) -> User:
        user_data_dict = body.model_dump()

        try:
            # Hash the password before saving it to the database
            user_data_dict["hashed_password"] = SecurityUtils.hash_password(
                body.hashed_password
            )

            new_user = User(**user_data_dict)
            self.db_session.add(new_user)
            await self.db_session.flush()
            return new_user
        except Exception as e:
            logger.error(f"Error creating user: {e}")

    async def get_by_id(self, user_id: uuid.UUID) -> Optional[User]:
        try:
            stmt = select(User).where(User.user_id == user_id)
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Error retrieving user: {e}")

    async def get_by_email(self, email: str) -> Optional[User]:
        try:
            stmt = select(User).where(User.user_email == email)
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Error retrieving user by email: {e}")

    async def get_list(self, pagination_params: PaginationParams):
        try:
            query = (
                select(User)
                .limit(pagination_params.limit)
                .offset((pagination_params.page - 1) * pagination_params.limit)
            )
            result = await self.db_session.execute(query)
            return result.scalars()
        except Exception as e:
            logger.error(f"Failed to get all users: {e}")

    async def update(self, user_id: uuid.UUID, **kwargs) -> User:
        try:
            user = await self.get_by_id(user_id)
            if not user:
                logger.warning(f"404: User {user} not found")
                raise HTTPException(status_code=404, detail="User not found")

            for key, value in kwargs.items():
                setattr(user, key, value)

            await self.db_session.flush()

            return user

        except Exception as e:
            logger.error(f"Failed to update user: {e}")

    async def delete(self, user_id: uuid.UUID):
        try:
            stmt = delete(User).filter(User.user_id == user_id).returning(User.user_id)
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Failed to delete user with ID {user_id}: {e}")
