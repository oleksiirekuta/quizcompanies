from sqlalchemy.ext.asyncio import AsyncSession
from abc import ABC, abstractmethod


class BaseDAL(ABC):
    """Data Access Layer for operating user info"""

    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    @abstractmethod
    async def create(self, **kwargs):
        pass

    @abstractmethod
    async def get_by_id(self, **kwargs):
        pass

    @abstractmethod
    async def get_by_email(self, **kwargs):
        pass

    @abstractmethod
    async def get_list(self, **kwargs):
        pass

    @abstractmethod
    async def update(self, **kwargs):
        pass

    @abstractmethod
    async def delete(self, **kwargs):
        pass
