import logging
import uuid
from typing import Optional

from fastapi import HTTPException
from sqlalchemy import delete, and_
from sqlalchemy.future import select
from starlette import status

from app.dal.base import BaseDAL
from app import schemas
from app.models import Company

logger = logging.getLogger(__name__)


class CompanyDAL(BaseDAL):
    async def create(
        self, body: schemas.CompanyCreate, current_user: uuid.UUID
    ) -> Company:
        user_data_dict = body.model_dump()
        user_data_dict["user_id"] = current_user

        try:
            new_company = Company(**user_data_dict)
            self.db_session.add(new_company)
            await self.db_session.flush()
            return new_company

        except Exception as e:
            logger.error(f"Error creating company: {e}")

    async def get_by_title(self, title: str) -> Optional[Company]:
        try:
            stmt = select(Company).where(
                and_(Company.company_title == title, Company.is_visible == True)  # noqa
            )
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Error retrieving company: {e}")

    async def get_by_id(self, company_id: int) -> Optional[Company]:
        try:
            stmt = select(Company).where(Company.company_id == company_id)
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Error retrieving company: {e}")

    async def get_by_email(self):
        pass

    async def get_list(self, pagination_params: schemas.PaginationParams):
        try:
            query = (
                select(Company)
                .where(Company.is_visible == True)  # noqa
                .limit(pagination_params.limit)
                .offset((pagination_params.page - 1) * pagination_params.limit)
            )
            result = await self.db_session.execute(query)
            return result.scalars()
        except Exception as e:
            logger.error(f"Failed to get all companies: {e}")

    async def get_own_companies(
        self, pagination_params: schemas.PaginationParams, current_user: uuid.UUID
    ):
        try:
            query = (
                select(Company)
                .where(Company.user_id == current_user)
                .limit(pagination_params.limit)
                .offset((pagination_params.page - 1) * pagination_params.limit)
            )
            result = await self.db_session.execute(query)
            return result.scalars()
        except Exception as e:
            logger.error(f"Failed to get all companies: {e}")

    async def update(self, company_id: int, **kwargs) -> Company:
        try:
            company = await self.get_by_id(company_id)
            if not company:
                logger.warning(f"404: Company with id {company_id} not found")
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail="Company not found"
                )

            for key, value in kwargs.items():
                setattr(company, key, value)

            await self.db_session.flush()

            return company

        except Exception as e:
            logger.error(f"Failed to update company: {e}")

    async def delete(self, company_id: int):
        try:
            stmt = (
                delete(Company)
                .filter(Company.company_id == company_id)
                .returning(Company.company_id)
            )
            result = await self.db_session.execute(stmt)
            return result.scalar_one_or_none()
        except Exception as e:
            logger.error(f"Failed to delete company with ID {company_id}: {e}")
