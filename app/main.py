import uvicorn
import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.core.config import app_settings
from app.api.routers import api_router

logger = logging.getLogger(__name__)
logging.basicConfig(
    filename="Quiz.log",
    level=logging.WARNING,
    format="%(levelname)s:%(asctime)s:%(name)s:%(message)s",
)

app = FastAPI()

app.include_router(api_router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=app_settings.ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if __name__ == "__main__":
    uvicorn.run(
        "app.main:app",
        host=app_settings.HOST,
        port=app_settings.PORT,
        reload=app_settings.RELOAD,
    )
