import uuid
from typing import Any

from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from app.dal.company import CompanyDAL
from app.dal.user import UserDAL
from app.models import User, UsersCompaniesActions, UserActionAssociation
from app.models.action import ActionTypeEnum
from app.services import AuthService

import logging

logger = logging.getLogger(__name__)


class ActionService:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def send_invite(
            self,
            company_id: int,
            receivers_email: list[str],
            current_user: User
    ) -> Any:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)
                user_dal = UserDAL(session)

                company = await company_dal.get_by_id(company_id)
                if not company:
                    logger.info(f"Company {company_id} not found")
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail="Company not found",
                    )

                await AuthService(self.db_session).check_user_permissions(
                    target_id=company.user_id, current_user=current_user
                )

                for receiver in receivers_email:
                    user = await user_dal.get_by_email(receiver)
                    if not user:
                        logger.warning(f"User {user.user_email} for "
                                       f"invitation not found."
                                       )
                        raise HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"User {user.user_email} not found",
                        )
                    invitation_action = UsersCompaniesActions(
                        action=ActionTypeEnum.invitation,
                        company_id=company_id
                    )

                    invitation_action.users.append(user)

                    self.db_session.add(invitation_action)
                    logger.info(f"Invitation for {user.user_email}"
                                f"was sent")

                await self.db_session.commit()

                return {"status": "ok"}

    async def check_invitation(self,
                               current_user: User
                               ) -> Any:
        async with (self.db_session as session):
            async with session.begin():

                try:
                    stmt = select(UsersCompaniesActions
                                  ).join(UserActionAssociation
                                         ).where(UserActionAssociation.user_id
                                                 == current_user.user_id)
                    run = await session.execute(stmt)
                    result = run.scalars().all()
                    if not result:
                        return HTTPException(
                            status_code=status.HTTP_404_NOT_FOUND,
                            detail="Invitation not found",
                        )
                    return result
                except Exception as e:
                    logger.error(f"Error retrieving invitation: {e}")

    async def cancel_invitation(
            self,
            from_company: int,
            invitation_receiver: uuid.UUID,
            current_user: User
    ) -> dict:
        async with self.db_session as session:
            company_dal = CompanyDAL(session)

            try:
                stmt = select(UsersCompaniesActions).join(UserActionAssociation).where(
                    UserActionAssociation.user_id == current_user.user_id,
                    UsersCompaniesActions.company_id == from_company)
                result = await session.execute(stmt)
                invitation = result.scalar()
            except Exception as e:
                logger.error(f"Error retrieving user: {e}")

            if not invitation:
                logger.info(f"Invitation for id {invitation_receiver} not found")
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Invitation not found",
                )

            company = await company_dal.get_by_id(from_company)
            if not company:
                logger.info(f"Company {from_company} not found")
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Company not found",
                )

            await AuthService(session).check_user_permissions(
                target_id=company.user_id, current_user=current_user
            )

            # for invite in invitation:
            #     await session.delete(invite)

            await session.delete(invitation)
            await session.commit()
            return {
                "status": 200
            }

    async def accept_invitation(
            self, company_id_sender: int,
            current_user: User
    ) -> Any:
        async with self.db_session as session:
            company_dal = CompanyDAL(session)
            user_dal = UserDAL(session)

            company = await company_dal.get_by_id(company_id_sender)
            if not company:
                logger.info(f"Company {company_id_sender} not found")
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail="Company not found",
                )

            await AuthService(session).check_user_permissions(
                target_id=company.user_id, current_user=current_user
            )

            try:
                invitations = select(UsersCompaniesActions).join(UserActionAssociation).where(
                    UserActionAssociation.user_id == current_user.user_id,
                    UsersCompaniesActions.company_id == company_id_sender,
                    UsersCompaniesActions.status == "pending"
                )
                result = await session.execute(invitations)
                invitations_list = result.scalars().all()
                if not invitations_list:
                    logger.info(f"No invitations found for company {company_id_sender}")
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail="Invitation not found",
                    )

                for invitation in invitations_list:
                    participant_user = await user_dal.get_by_id(current_user.user_id)
                    company.participants.append(participant_user)
                    await session.delete(invitation)

                await session.commit()
                return {
                    "status": 200
                }
            except Exception as e:
                logger.error(f"Error retrieving invitation: {e}")
                return {
                    "status": 400,
                    "detail": "Error retrieving invitation. Try again later."
                }
