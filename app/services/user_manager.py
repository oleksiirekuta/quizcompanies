import uuid
import jwt
from jwt import PyJWTError
from typing import List

from fastapi import HTTPException, Header
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from app.models import User
from app import schemas
from app.dal.user import UserDAL
from app.core.security import SecurityUtils
from app.core.config import auth_settings

import logging

logger = logging.getLogger(__name__)


class UserManager:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create_new_user(
            self, body: schemas.UserSignUpRequestModel
    ) -> User:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                existing_user = await user_dal.get_by_email(body.user_email)
                if existing_user:
                    raise HTTPException(
                        status_code=400,
                        detail="Email already exists"
                    )

                return await user_dal.create(body)

    async def get_user_by_id(self, user_id: uuid.UUID) -> User:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                user = await user_dal.get_by_id(user_id)
                if not user:
                    raise HTTPException(
                        status_code=404,
                        detail='User not found'
                    )

                return user

    async def get_all(
        self, pagination_params: schemas.PaginationParams
    ) -> List[schemas.UserBase]:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                return await user_dal.get_list(pagination_params)

    async def update_user(
        self,
        user_id: uuid.UUID,
        body: schemas.UserUpdateRequestModel,
        current_user: schemas.UserBase,
    ) -> User:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)

                await AuthService(self.db_session).check_user_permissions(
                    target_id=user_id, current_user=current_user
                )

                return await user_dal.update(user_id, **body.model_dump())

    async def update_user_password(
        self,
        user_id: uuid.UUID,
        old_password: str,
        new_password: str,
        current_user: schemas.UserBase,
    ):
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                user = await user_dal.get_by_id(user_id)

                await AuthService(self.db_session).check_user_permissions(
                    target_id=user_id, current_user=current_user
                )

                if not SecurityUtils.verify_password(
                    old_password, user.hashed_password
                ):
                    logger.info(
                        f"User {user_id}: old password != new password"
                    )
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail="Old password verification failed.",
                    )

                user.hashed_password = SecurityUtils.hash_password(
                    new_password
                )
                await session.flush()
                return user

    async def delete_user(
            self, user_id: uuid.UUID,
            current_user: schemas.UserBase
    ):
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)

                await AuthService(self.db_session).check_user_permissions(
                    target_id=user_id, current_user=current_user
                )

                return await user_dal.delete(user_id)


class AuthService:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def authenticate_user(self, user_email: str, password: str):
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)

                existing_user = await user_dal.get_by_email(user_email)
                if not existing_user:
                    logger.warning(f"401: User {user_email} not found")
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED,
                        detail="User not found. Check again."
                    )

                if not SecurityUtils.verify_password(
                        password, existing_user.hashed_password
                ):
                    logger.warning(
                        f"401: Password for {user_email} is incorrect."
                    )
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED,
                        detail=f"Error: The password you entered "
                               f"for the email address {existing_user} is "
                               f"incorrect. Try again.")

                access_token = SecurityUtils.create_access_token(
                    data={"sub": user_email}
                )
                return access_token

    async def get_current_user_from_token(
            self, token: str = Header(None)
    ) -> User | None:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                try:
                    if not token:
                        logger.warning(f'Token not found - {token}')
                        raise HTTPException(
                            status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Couldn't validate credentials",
                        )

                    payload = jwt.decode(
                        token,
                        auth_settings.SECRET_KEY,
                        algorithms=[auth_settings.ALGORITHM]
                    )

                    user_email: str = payload.get("sub")
                    logger.info(f"Email extracted is {user_email}")

                    user = await user_dal.get_by_email(user_email)
                    if user is not None:
                        return user

                except PyJWTError as e:
                    logger.warning(f"Couldn't validate credentials: {e}")
        return await self.get_current_user_from_auth0token(token)

    async def get_current_user_from_auth0token(
            self, token
    ) -> User | None:
        async with self.db_session as session:
            async with session.begin():
                user_dal = UserDAL(session)
                try:
                    credential_exception = HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED,
                        detail="Couldn't validate credentials",
                    )

                    payload = jwt.decode(
                        token,
                        auth_settings.AUTH0_SIGNIN_SECRET,
                        audience=auth_settings.API_AUDIENCE,
                        algorithms=auth_settings.ALGORITHM
                    )

                    user_email: str = payload.get("auth0_email")

                    logger.info(f"Email extracted is {user_email}")

                    if user_email is None:
                        logger.warning(
                            f"User email does not exist: {user_email}."
                        )
                        raise credential_exception

                except PyJWTError as e:
                    logger.warning(f"Couldn't validate credentials: {e}")
                    raise credential_exception

                user = await user_dal.get_by_email(user_email)
                if not user:
                    logger.warning(
                        f"User email does not exist:"
                        f" {user_email}. Creating ..."
                    )

                    user_pass: str = payload.get("iat")

                    user_data = schemas.UserSignUpRequestModel(
                        user_email=user_email,
                        hashed_password=str(user_pass),
                        user_firstname="Unknown",
                        user_lastname="Unknown",
                        user_city="",
                        user_phone="",
                        user_links="",
                        user_avatar="",
                    )

                    return await user_dal.create(user_data)

                return user

    async def check_user_permissions(
            self, target_id: uuid.UUID,
            current_user: User
    ) -> bool:
        if current_user.user_id != target_id:
            logger.warning(
                f"403: User {current_user.user_id} "
                f"tried to delete {target_id}"
            )
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="Access denied!"
            )
        return True
