import uuid
from typing import List

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

import logging

from starlette import status

from app import schemas
from app.dal.company import CompanyDAL
from app.models import Company
from app.services import AuthService

logger = logging.getLogger(__name__)


class CompanyService:
    def __init__(self, db_session: AsyncSession):
        self.db_session = db_session

    async def create_new_company(
        self, current_user: schemas.UserBase, body: schemas.CompanyCreate
    ) -> Company:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)

                existing_company = await company_dal.get_by_title(body.company_title)

                if existing_company:
                    raise HTTPException(
                        status_code=status.HTTP_400_BAD_REQUEST,
                        detail="Company already exists",
                    )

                return await company_dal.create(body, current_user=current_user.user_id)

    async def get_company_by_title(self, title: str) -> Company:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)
                company = await company_dal.get_by_title(title)
                if not company:
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail="Company not found",
                    )

                return company

    async def get_list(
        self, pagination_params: schemas.PaginationParams
    ) -> List[schemas.CompanyDetailResponse]:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)

                return await company_dal.get_list(pagination_params=pagination_params)

    async def get_my_companies(
        self, pagination_params: schemas.PaginationParams, current_user: uuid.UUID
    ) -> List[schemas.CompanyDetailResponse]:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)

                return await company_dal.get_own_companies(
                    pagination_params=pagination_params, current_user=current_user
                )

    async def update_company(
        self, company_id: int, body: schemas.CompanyUpdate, current_user
    ) -> Company:
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)

                company = await company_dal.get_by_id(company_id)
                if not company:
                    logger.info(f"Company {company_id} not found")
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail="Company not found",
                    )

                await AuthService(self.db_session).check_user_permissions(
                    target_id=company.user_id, current_user=current_user
                )

                return await company_dal.update(company_id, **body.model_dump())

    async def delete_company(self, company_id: int, current_user):
        async with self.db_session as session:
            async with session.begin():
                company_dal = CompanyDAL(session)

                company = await company_dal.get_by_id(company_id)
                if not company:
                    logger.info(f"Company {company_id} not found")
                    raise HTTPException(
                        status_code=status.HTTP_404_NOT_FOUND,
                        detail="Company not found",
                    )

                await AuthService(self.db_session).check_user_permissions(
                    target_id=company.user_id, current_user=current_user
                )

                return await company_dal.delete(company_id)

    