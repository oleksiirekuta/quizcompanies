"""added field to actions

Revision ID: 8080024a0945
Revises: 568c2e479c60
Create Date: 2023-11-01 19:58:43.194460

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '8080024a0945'
down_revision: Union[str, None] = '568c2e479c60'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user_company_action',
                  sa.Column('company_id',
                            sa.Integer(), nullable=False)
                  )
    op.create_foreign_key("fk_company_id_company_id",
                          'user_company_action',
                          'companies',
                          ['company_id'],
                          ['company_id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(
        "fk_company_id_company_id",
        'user_company_action', type_='foreignkey')
    op.drop_column('user_company_action', 'company_id')
    # ### end Alembic commands ###
