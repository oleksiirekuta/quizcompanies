# internshipOR-Med

Welcome to Oleksii Rekuta internship project.

To start the project, you need to take the following steps:

1. Clone the repository:
   ```bash
   git clone https://github.com/alexsproject/intershipOR-Med.git
   ```
2. Navigate to the project directory:
   ```bash
   cd intershipOR-Med
   ```
3. Install the project requirements
   ```bash
   pip install -r requirements/base_req.txt
   ```
4. Run the project
   ```bash
   python -m app.main
   ```
   
## Running the Application within Docker

To run the application within a Docker container, 
follow these steps:

1. Build the Docker image for the application using the provided Dockerfile:
   ```bash
   docker build -t my-fastapi-app .
   ```
2. Start a Docker container with the application:
   ```bash
   docker run -p 8000:8000 -d my_fastapi_app
   ```
The application should now be accessible at http://localhost:8000 in your web browser.

## Running Tests within Docker

To run tests for the application within a Docker container, follow these steps:
1. Build a Docker image for running tests using the provided Dockerfile for tests:
   ```bash
   docker build -t my_fastapi_app_tests -f Dockerfile.tests .
   ```

2. Execute the tests within a Docker container:
   ```bash
   docker run --rm my_fastapi_app_tests
   ```

The tests will run, and you'll see the test results in the terminal.
That's it! You've successfully launched the application within Docker and run tests in a Docker container.

## Set Up Migrations
If you have made changes to the database, generate migrations using this command:
```bash
alembic revision --autogenerate -m "What was changed?"
```

Apply the migration to your database to bring it up to the current schema:
```bash
alembic upgrade head
```
#### Congratulations! The database is ready for use.

## Running the Application within docker-compose file

In the root directory, run the command:
```bash
docker compose -f docker-compose.yaml up -d
```
Now you can apply migrations and use the application.

To stop all containers, run the command:
```bash
docker compose -f docker-compose.yaml down && docker network prune --force
```
